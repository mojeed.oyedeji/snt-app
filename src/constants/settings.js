export const uri =
  {
    test: "http://127.0.0.20",
    dev: "https://api.chetaa.me",
    live: "https://snt-api.thelastenvoy.com",
    mode: "test",
  }
