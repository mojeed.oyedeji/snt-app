import React, { useEffect } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import ListItem from '@mui/material/ListItem';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import { getCustomers } from '../../actions/customer';


const classes = {
    root:{
        marginTop:10
    }
}

export default function Main(){

    const dispatch = useDispatch();
    const customers = useSelector(state => state.customers.customers);

    useEffect(() => {
        dispatch(getCustomers())
    }, [])

    return(
      <div style={{marginTop:10}} className={classes.root}>
      <Grid justifyContent="center" alignItems="center" container direction="row">
        <Grid item xs={12} md={8} style={{marginTop:0, padding:10}}>
            <Card variant="outlined" color="primary">
            <CardContent>
            <Typography color="primary" variant="h4">Customers</Typography>
            <Typography color="textSecondary" variant="body1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's 
            standard dummy text ever since the 1500s, when an 
            unknown printer took a galley of type and scrambled it to make a type specimen book.</Typography>
            </CardContent>
            </Card>


            <div style={{marginTop:20}}>
            <Grid container direction="row" spacing={1}>
            <Grid item md={6}>
            <Typography variant="h6"> Active ({customers.filter((item) => item.active == 1).length}) </Typography>
            {customers.map((item) => (
                item.active == 1 &&
                <ListItem style={{padding:0}} divider>
                     <Item data={item} />
                </ListItem>
               
            ))}
            </Grid>
            <Grid item md={6}>
            <Typography variant="h6"> Non-Active ({customers.filter((item) => item.active != 1).length}) </Typography>
            {customers.map((item) => (
                item.active != 1 &&
                <ListItem style={{padding:0}} divider>
                     <Item data={item} />
                </ListItem>
               
            ))}
            </Grid>
            </Grid>
            </div>
            
            
      
       



        

        
        
       
        </Grid>
      </Grid>
           
        </div>
    )
}

function Item(props){
    return(
        <Card style={{width:"100%"}}>
            <CardContent>
                <Typography variant="h6"> {props.data.first_name + " " + props.data.last_name} </Typography>
                <Typography variant="body1"> {props.data.active == 1 ? "active" : "inactive"} </Typography>
            </CardContent>
        </Card>
    )
}