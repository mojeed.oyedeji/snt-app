import React from 'react';
import {useHistory} from 'react-router-dom';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';


const classes = {
    root:{
        marginTop:10
    }
}

export default function Main(){
    const history = useHistory();


    return(
      <div className={classes.root}>
      <Grid justifyContent="center" alignItems="center" container direction="row">
        <Grid item xs={12} md={8} style={{marginTop:0, padding:10}}>
            <Card variant="outlined" color="primary">
            <CardContent>
            <Grid container direction="row" spacing={2}>
            <Grid item md={6}>
            <Typography  variant="h4"><b>Task 1</b></Typography>
            <Typography color="textSecondary" variant="body1">
                The first task requires listing active and non active users (customers) in a database.
                There should be a drag and drop feature to move a user between active and non-active categories.
                The active attribute of the user should update in the database with respect to changes made in the UI.
            </Typography>
            <section style={{marginTop:10}}>
            <a href="/customers"> View </a>
            </section>
            
            </Grid>
            <Grid item md={6}>
            <Typography variant="h4"><b>Task 2 </b></Typography>
            <Typography color="textSecondary" variant="body1">
            The second task requires building a dashboard for the film class. Some suggestions were to show on the dashboard how much clients paid for films, quantity of films by languages, quantity of films in every category and rating the top 10 users 
            </Typography>
            <a href="/dashboard"> View </a>
            </Grid>

        </Grid>
        </CardContent>
        </Card>
      
    
        </Grid>
      </Grid>
           
        </div>
    )
}