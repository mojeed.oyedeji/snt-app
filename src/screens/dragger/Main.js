import React, { useEffect, useRef, useState } from 'react';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import ListItem from '@mui/material/ListItem';
import CardContent from '@mui/material/CardContent';
import { useDispatch, useSelector } from 'react-redux';
import { getCustomers } from '../../actions/customer';


const classes = {
    root:{
        marginTop:10
    }
}

export default function Main(){

    const dispatch = useDispatch();
    const customers = useSelector(state => state.customers.customers);
    const [source, setSource] = useState([]);
    const [destination, setDestination] = useState([]);

    const dragItem = useRef();
    const dragOverItem = useRef();
    
    const [sourceList, setSourceList] = useState(['Item 1','Item 2','Item 3','Item 4','Item 5','Item 6']);
    const [destinationList, setDestinationList] = useState(['Item 7','Item 8','Item 9','Item 10','Item 11','Item 12']);
    
    

    function removeFromList(list, item){

    }

    function addToList(list, item){


    }

    

    const dragStart = (e, position) => {
        dragItem.current = position;
        console.log(e.target.innerHTML);
        setSource(e.target.parentNode.id);
      };
     
      const dragEnter = (e, position) => {
        dragOverItem.current = position;
        console.log(e.target.innerHTML);
        setDestination(e.target.parentNode.id);
      };
     
      const drop = (e, list) => {
        if(source == destination){
            if(list == "list1"){
                const copyListItems = [...sourceList];
                const dragItemContent = copyListItems[dragItem.current];
                copyListItems.splice(dragItem.current, 1);
                copyListItems.splice(dragOverItem.current, 0, dragItemContent);
                dragItem.current = null;
                dragOverItem.current = null;
                setSourceList(copyListItems);
                console.log(e.target.parentNode.id)
            }else{
                const copyListItems = [...destinationList];
                const dragItemContent = copyListItems[dragItem.current];
                copyListItems.splice(dragItem.current, 1);
                copyListItems.splice(dragOverItem.current, 0, dragItemContent);
                dragItem.current = null;
                dragOverItem.current = null;
                setDestinationList(copyListItems);
            }
        }else{
            if(source == "list1"){
                const item = e.target.innerHTML;
                console.log(item)
                var newSource = sourceList.pop(item);
                setDestinationList([...destinationList, item]);
                setSourceList(sourceList.filter((it) => it != item));
                //setSourceList(newSource)
            }else{
                const item = e.target.innerHTML;
                setSourceList([...sourceList, item]);
                setDestinationList(destinationList.filter((it) => it != item));
            }
        }
       
        
      };

    useEffect(() => {
        dispatch(getCustomers())
    }, [])

    return(
      <div style={{marginTop:10}} className={classes.root}>
      <Grid justifyContent="center" alignItems="center" container direction="row">
        <Grid item xs={12} md={8} style={{marginTop:0, padding:10}}>
            <Card variant="outlined" color="primary">
            <CardContent>
            <Typography color="primary" variant="h4">Customers</Typography>
            <Typography color="textSecondary" variant="body1">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
            Lorem Ipsum has been the industry's 
            standard dummy text ever since the 1500s, when an 
            unknown printer took a galley of type and scrambled it to make a type specimen book.</Typography>
            </CardContent>
            </Card>

            <Grid container direction="row">
                <Grid item md={6}>  
                <div id="list1">  
                {sourceList && sourceList.map((item, index) => (
                <div style={{backgroundColor:'lightblue', margin:'10px ', textAlign:'center', fontSize:'20px'}}
                    onDragStart={(e) => dragStart(e, index)}
                    onDragEnter={(e) => dragEnter(e, index)}
                    onDragEnd={(e) => drop(e, 'list1')}
                    key={index}
                    draggable>
                    {item}
                </div>
                ))}
                </div>
                </Grid>
                <Grid item md={6}>
                <div id="list2">
                {destinationList && destinationList.map((item, index) => (
                <div style={{backgroundColor:'lightblue', margin:'10px ', textAlign:'center', fontSize:'20px'}}
                    onDragStart={(e) => dragStart(e, index)}
                    onDragEnter={(e) => dragEnter(e, index)}
                    onDragEnd={(e) => drop(e, 'list2')}
                    key={index}
                    draggable>
                    {item}
                </div>
                 ))}
                </div>
                
               
                </Grid>
            </Grid>


           
            
            
      
       



        

        
        
       
        </Grid>
      </Grid>
           
        </div>
    )
}

function Item(props){
    return(
        <Card style={{width:"100%"}}>
            <CardContent>
                <Typography variant="h6"> {props.data.first_name + " " + props.data.last_name} </Typography>
                <Typography variant="body1"> {props.data.active == 1 ? "active" : "inactive"} </Typography>
            </CardContent>
        </Card>
    )
}