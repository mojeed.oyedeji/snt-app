import { createTheme, } from '@mui/material/styles';

export const THEME = createTheme({
   typography: {
    "fontFamily": "\"Urbanist\", sans-serif",
    "fontSize": 14,
    "fontWeightLight": 300,
    "fontWeightRegular": 400,
    "fontWeightMedium": 500,
    "fontWeightBold": 700,
  },
  palette: {
   primary: {
    main:'#03045e'
   },
 },
});
